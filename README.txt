# Install global vue cli
npm install -g @vue/cli

# Create a project vue
vue create my-project

# vuetify plugin
vue add vuetify

# Font Awesome 5 Icons
yarn add @fortawesome/fontawesome-free -D

# Axios
yarn add axios






