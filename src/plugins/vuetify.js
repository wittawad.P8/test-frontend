import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

import "@fortawesome/fontawesome-free/css/all.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#d9ef0c",
        accent: "#d9ef0c",
      },
      dark: {
        primary: "#d9ef0c",
        accent: "#d9ef0c",
      },
    },
  },
  icons: {
    iconfont: "fa",
  },
});
